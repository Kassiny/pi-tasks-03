#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 256

void fillArr(int *arr, int size, int diapozon)
{
    int i;
    for(i=0;i<size;i++)
    {
        arr[i]=rand()%diapozon;
    }
}
void printArr(int *arr, int size, int diapozon)
{
    int i;
    for(i=0;i<size;i++)
    {
        printf("%d ",arr[i]);
    }
    printf("\n");
}
int maxScuence(int *arr, int size)
{
    int sec=1, prev,i, secMax = -1;
    prev=arr[0];
    for(i =1; i<size;i++)
    {
        prev = arr[i-1];
        if(arr[i]==prev)
        {
            sec++;
            if (sec>secMax) secMax=sec;
        }
        else
        {
            sec=1;

        }
    }
   return secMax;
}

int main()
{
    srand(time(0));
    int len=0;
    int mas[N];
    int diap=10;
    fillArr(mas,N,diap);
    printf("%d \n", maxScuence(mas,N));
    return 0;
}

