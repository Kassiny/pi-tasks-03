//Написать программу, которая генерирует случайным образом массив целых чисел,\
определяет в нем максимальное, минимальное и среднее значения. Реализовать алгоритм в виде отдельной функции.
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 10

void fillArray (int *arr,int size, int maxElement)
{
    int i;
    for(i=0;i<size;i++)
    {
        arr[i]=rand()%maxElement;
    }
}

int findMax (int *arr, int size)
{
    int max=-INT_MAX,i;
    for(i=0;i<size;i++)
    {
        if (arr[i]>max)
        {
            max=arr[i];
        }
    }
    return max;
}

int findMin (int *arr, int size)
{
    int min=INT_MAX,i;
    for(i=0;i<size;i++)
    {
        if (arr[i]<min)
        {
            min=arr[i];
        }
    }
    return min;
}

float median (int *arr, int size)
{
    int sum=0,i;
    for(i=0;i<size;i++)
    {
        sum+=arr[i];
    }
    return (float)sum/(float)size;
}

void  idenifitytArray (int* arr, int size, int* min, int* max, float* med, int maxElement)
{
    fillArray(arr,size, maxElement);
    *min=findMin(arr,size);
    *max=findMax(arr,size);
    *med=median(arr,size);
}

int main()
{
    srand(time(0));
    int mass[N];
    int max,min,maxElement=100;
    float med;
    idenifitytArray(mass,N,&min,&max,&med,maxElement);
    printf("maximum El: %d, Minimum: %d Median: %f \n",max,min,med);
    return 0;
}
